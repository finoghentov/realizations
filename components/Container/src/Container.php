<?php

namespace Finoghentov\Container;

use Closure;
use Finoghentov\Container\Contracts\ContainerInterface;
use Finoghentov\Container\Exceptions\BindingException;
use ReflectionException;
use ReflectionParameter;

class Container implements ContainerInterface
{
    /**
     * @var ?ContainerInterface
     */
    static private ?ContainerInterface $instance;

    /**
     * @var array
     */
    protected array $instances = [];

    /**
     * @var array
     */
    protected array $bindings = [];

    /**
     * @var array
     */
    protected array $aliases = [];

    /**
     * @var array
     */
    protected array $with = [];

    /**
     * Container constructor.
     *
     * @param array $concretes
     * @throws ReflectionException|BindingException
     */
    private function __construct(array $concretes = [])
    {
        $this->fillCoreInstances($concretes);
    }

    /**
     * Get resolved instance
     *
     * @param string $id
     * @return mixed
     */
    public function get(string $id)
    {
        return $this->has($id)
            ? $this->instances[$id] : null;
    }

    /**
     * @param string $id
     * @return bool
     */
    public function has(string $id): bool
    {
        return isset($this->instances[$id]);
    }

    /**
     * @param string $abstract
     * @param array $parameters
     * @return mixed
     * @throws ReflectionException|BindingException
     */
    public function make(string $abstract, array $parameters = [])
    {
        $this->with($parameters);

        $resolved = $this->resolve($abstract);

        $this->unboundWith();

        return $resolved;
    }

    /**
     * @param string $abstract
     * @return mixed
     * @throws ReflectionException|BindingException
     */
    protected function resolve(string $abstract)
    {
        $abstract = $this->getAlias($abstract);

        if (isset($this->instances[$abstract])) {
            return $this->instances[$abstract];
        }

        $concrete = $this->getConcrete($abstract);

        if ($this->isBuildable($abstract, $concrete)) {
            $object = $this->build($concrete);
        } else {
            $object = $this->resolve($concrete);
        }

        if ($this->isSingleton($abstract)) {
            $this->instances[$abstract] = $object;
        }

        return $object;
    }

    /**
     * @param $concrete
     * @return mixed
     * @throws BindingException
     * @throws ReflectionException
     */
    public function build($concrete)
    {
        if ($concrete instanceof Closure) {
            return $concrete($this);
        }

        try {
            $reflector = new \ReflectionClass($concrete);
        } catch (ReflectionException $exception) {
            throw new BindingException("Target class [$concrete] does not exists");
        }

        if (!$reflector->isInstantiable()) {
            throw new BindingException("Target class [$concrete] is not instantiable");
        }

        $constructor = $reflector->getConstructor();

        if (!$constructor) {
            return new $concrete;
        }

        $dependencies = $constructor->getParameters();

        $instances = $this->resolveDependencies($dependencies);

        return $reflector->newInstanceArgs($instances);
    }

    /**
     * @param array $dependencies
     * @return array
     * @throws ReflectionException|BindingException
     */
    protected function resolveDependencies(array $dependencies): array
    {
        $result = [];

        /**
         * @var ReflectionParameter $dependency
         */
        foreach ($dependencies as $dependency) {
            if (key_exists($dependency->getName(), $this->with)) {
                $result[] = $this->with[$dependency->getName()];
                unset($this->with[$dependency->getName()]);
            } else if ($dependency->isDefaultValueAvailable()) {
                $result[] = $dependency->getDefaultValue();
                continue;
            } else {
                $result[] = $this->resolve($dependency->getClass()->getName());
            }

        }

        return $result;
    }

    /**
     * @param string $alias
     * @param string $abstract
     */
    public function alias(string $alias, string $abstract): void
    {
        $this->aliases[$alias] = $abstract;
    }

    /**
     * @param $abstract
     * @return mixed
     */
    public function getAlias($abstract)
    {
        return $this->aliases[$abstract] ?? $abstract;
    }

    /**
     * @param $abstract
     * @return Closure|string
     */
    protected function getConcrete($abstract)
    {
        if (isset($this->bindings[$abstract])) {
            return $this->bindings[$abstract]['concrete'];
        }

        return $abstract;
    }

    /**
     * @param string $abstract
     * @param Closure|string|null $concrete
     * @param bool $singleton
     */
    public function bind(string $abstract, $concrete = null, bool $singleton = false): void
    {
        $this->dropStaleInstances($abstract);

        if (is_null($concrete)) {
            $concrete = $abstract;
        }

        if (!$concrete instanceof Closure) {
            if (!is_string($concrete)) {
                throw new \TypeError(self::class . '::bind(): Argument #2 ($concrete) must be type of Closure|string|null');
            }

            $concrete = $this->getClosure($abstract, $concrete);
        }

        $this->bindings[$abstract] = compact('concrete', 'singleton');
    }

    /**
     * @param $abstract
     * @param $concrete
     * @return Closure
     */
    protected function getClosure($abstract, $concrete): Closure
    {
        return function($container) use ($abstract, $concrete) {
            if ($abstract === $concrete) {
                return $container->build($concrete);
            }

            return $container->resolve($concrete);
        };
    }

    /**
     * @param string $abstract
     * @return bool
     */
    protected function isSingleton(string $abstract): bool
    {
        return isset($this->bindings[$abstract]) ?
            $this->bindings[$abstract]['singleton'] : false;
    }

    /**
     * @param $abstract
     * @param $concrete
     * @return bool
     */
    protected function isBuildable($abstract, $concrete): bool
    {
        return $concrete instanceof Closure || $abstract === $concrete;
    }

    /**
     * @param array $concretes
     * @throws ReflectionException|BindingException
     */
    protected function fillCoreInstances(array $concretes)
    {
        foreach ($concretes as $concrete) {
            $this->instances[$concrete] = $this->make($concrete);
        }
    }

    /**
     * @param string $abstract
     */
    protected function dropStaleInstances(string $abstract)
    {
        unset($this->bindings[$abstract], $this->instances[$abstract]);
    }

    /**
     * Build new container instance
     *
     * @param array $concretes
     * @return ContainerInterface
     * @throws ReflectionException|BindingException
     */
    public static function buildContainer(array $concretes = []): ContainerInterface
    {
        if (isset(self::$instance)) {
            return self::$instance;
        }

        return self::$instance = new static($concretes);
    }

    /**
     * Get container singleton instance
     *
     * @return ContainerInterface
     */
    public static function getInstance(): ContainerInterface
    {
        return self::$instance;
    }

    /**
     * Unset container instance
     *
     * @return void
     */
    public static function unsetInstance(): void
    {
        self::$instance = null;
    }

    /**
     * Setting parameters for resolving
     *
     * @param array $parameters
     */
    public function with(array $parameters = []): void
    {
        $this->with = array_merge($this->with, $parameters);
    }

    /**
     * Unbound parameters after resolving
     */
    public function unboundWith(): void
    {
        $this->with = [];
    }

    /**
     * @param Closure|array $closure
     * @param array $parameters
     * @return mixed
     */
    public function call($closure, array $parameters = [])
    {

    }
}
