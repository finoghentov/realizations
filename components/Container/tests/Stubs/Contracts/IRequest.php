<?php


namespace Finoghentov\Container\Tests\Stubs\Contracts;


interface IRequest
{
    /**
     * Get all data from request.
     */
    public function all(): array;

    /**
     * Get request url.
     */
    public function url(): string;
}
