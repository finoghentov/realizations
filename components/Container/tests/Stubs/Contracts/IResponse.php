<?php


namespace Finoghentov\Container\Tests\Stubs\Contracts;


interface IResponse
{
    /**
     * Get response status
     */
    public function getStatus();

    /**
     * Set response status
     * @param int $status
     */
    public function setStatus(int $status);
}
