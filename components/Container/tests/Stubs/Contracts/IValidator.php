<?php


namespace Finoghentov\Container\Tests\Stubs\Contracts;


interface IValidator
{
    public function validate();
}
