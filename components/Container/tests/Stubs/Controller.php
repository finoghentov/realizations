<?php


namespace Finoghentov\Container\Tests\Stubs;


use Finoghentov\Container\Tests\Stubs\Contracts\IRequest;

class Controller
{
    /**
     * @param IRequest $request
     * @return IRequest
     */
    public function action(IRequest $request): IRequest
    {
        return $request;
    }
}
