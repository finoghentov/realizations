<?php


namespace Finoghentov\Container\Tests\Stubs;


class DatabaseConnection
{
    /**
     * @var DatabaseDriver
     */
    private DatabaseDriver $db_driver;

    /**
     * @var string
     */
    protected string $host;

    /**
     * @var string
     */
    protected string $user;

    /**
     * @var string
     */
    protected string $password;

    /**
     * @var string
     */
    protected string $driver;

    /**
     * DatabaseConnection constructor.
     *
     * @param DatabaseDriver $db_driver
     * @param string $host
     * @param string $user
     * @param string $password
     * @param string $driver
     */
    public function __construct(
        DatabaseDriver $db_driver,
        string $host = '127.0.0.1',
        string $user = 'root',
        string $password = 'root',
        string $driver = 'mysql'
    ) {
        $this->db_driver = $db_driver;
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->driver = $driver;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getDriver(): string
    {
        return $this->driver;
    }
}
