<?php


namespace Finoghentov\Container\Tests\Stubs;


use Finoghentov\Container\Tests\Stubs\Contracts\IRequest;
use Finoghentov\Container\Tests\Stubs\Contracts\IValidator;

class HttpRequest implements IRequest
{
    /**
     * @var IValidator
     */
    private IValidator $validator;

    /**
     * HttpRequest constructor.
     *
     * @param IValidator $validator
     */
    public function __construct(IValidator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Get all data from request.
     */
    public function all(): array
    {
        // TODO: Implement all() method.
    }

    /**
     * Get request url.
     */
    public function url(): string
    {
        // TODO: Implement url() method.
    }
}
