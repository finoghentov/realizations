<?php

namespace Finoghentov\Container\Tests\Stubs;

use Finoghentov\Container\Tests\Stubs\Contracts\IResponse;

class JsonResponse implements IResponse
{
    /**
     * Get response status
     */
    public function getStatus()
    {
        // TODO: Implement getStatus() method.
    }

    /**
     * Set response status
     * @param int $status
     */
    public function setStatus(int $status)
    {
        // TODO: Implement setStatus() method.
    }
}
