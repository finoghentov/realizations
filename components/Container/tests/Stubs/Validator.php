<?php


namespace Finoghentov\Container\Tests\Stubs;


use Finoghentov\Container\Tests\Stubs\Contracts\IValidator;

class Validator implements IValidator
{
    public function validate() { }
}
